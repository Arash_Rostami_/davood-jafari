-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2021 at 06:00 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dj`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_project` text COLLATE utf8mb4_unicode_ci,
  `text_project` longtext COLLATE utf8mb4_unicode_ci,
  `date_project` longtext COLLATE utf8mb4_unicode_ci,
  `audio_address` longtext COLLATE utf8mb4_unicode_ci,
  `title_project_f` text COLLATE utf8mb4_unicode_ci,
  `text_project_f` longtext COLLATE utf8mb4_unicode_ci,
  `date_project_f` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title_project`, `text_project`, `date_project`, `audio_address`, `title_project_f`, `text_project_f`, `date_project_f`, `created_at`, `updated_at`) VALUES
(1, 'Projects', NULL, NULL, NULL, 'پروژه ها', NULL, NULL, NULL, NULL),
(2, NULL, 'Selat for four voices', '2021', ' ', NULL, 'سلات چهار صدا', '2021', '2021-04-19 02:28:35', '2021-04-19 02:28:35'),
(3, NULL, 'Sheshgah for six accordions and electronic', '2020', 'audio/', NULL, 'ششگاه برای شش آکاردیون و الکترونیک', '2020', '2021-04-19 03:06:27', '2021-04-19 03:06:27'),
(4, NULL, 'Seven cities of Persian music for string quartet, Iranian quartet and voice five movements and four interludes', '2020', 'audio/projects/', NULL, 'هفت شهر موسیقی ایرانی برای کوارتت زهی ، کوارتت ایرانی و صدای پنج حرکت و چهار مدخل', '2020', '2021-04-19 03:08:25', '2021-04-19 03:08:25'),
(5, NULL, '176 for symphonic orchestra', '2020', 'audio/projects/', NULL, '176 برای ارکستر سمفونیک', '2020', '2021-04-19 03:10:26', '2021-04-19 03:10:26'),
(6, NULL, 'Whirling cycles for percussion ensemble', '2020', 'audio/projects/607c92dba12d7end-of-affair-audio.mp3', NULL, 'چرخه های چرخشی برای گروه کوبه ای', '2020', '2021-04-19 03:13:15', '2021-04-19 03:13:15'),
(8, NULL, 'Zoorkhaneh for Iranian ensemble, viola, percussion, singer, dancer and video', '2020', 'audio/projects/607c932e8a10cShadmehr-Aghili-Bi-Ehsas.mp3', NULL, 'زورخانه برای گروه ایرانی ، ویولا ، سازهای کوبه ای ، خواننده ، رقصنده و ویدیو', '2020', '2021-04-19 03:14:38', '2021-04-19 03:14:38'),
(10, NULL, 'Abeyance solo for the cello', '2020', 'audio/projects/607c984ef3c7eBefour (Zayn Malik)-(SpaceMaza.com).mp3', NULL, 'انفرادی انفرادی برای ویولن سل', '2020', '2021-04-19 03:36:31', '2021-04-19 03:36:31'),
(11, NULL, 'From the heart of the desert for oboe and harp', '2020', 'audio/projects/607c98b826d3bal035_mind_against_walking_away_2981475022800627242.mp3', NULL, 'از دل کویر برای آبوا و چنگ', '2020', '2021-04-19 03:38:16', '2021-04-19 03:38:16'),
(12, NULL, 'Sound poetry no for solo voice', '2019', 'audio/projects/607c9c015401fend-of-affair-audio.mp3', NULL, 'شعر صدا نه برای صدای انفرادی', '2019', '2021-04-19 03:52:17', '2021-04-19 03:52:17'),
(13, NULL, 'Sound poetry no 1 for voice, Braille note taker, and electronic', '2019', ' ', NULL, 'شعر صدا شماره 1 برای صدا ، یادداشت بریل و الکترونیکی', '2019', '2021-04-19 03:54:24', '2021-04-19 03:54:24'),
(14, NULL, 'Panjgah for flutes family', '2019', ' ', NULL, 'پنجگاه برای خانواده فلوت', '2019', '2021-04-19 10:02:14', '2021-04-19 10:02:14'),
(15, NULL, 'Iranian colors for chamber ensemble', '2019', 'audio/projects/607cf2f14d59fal035_mind_against_walking_away_2981475022800627242.mp3', NULL, 'رنگهای ایرانی برای گروه مجلسی', '2019', '2021-04-19 10:03:13', '2021-04-19 10:03:13'),
(16, NULL, 'Dargha for soprano, harp, clarinet and double bass', '2019', NULL, NULL, 'درگه برای سوپرانو ، چنگ ، ​​کلارینت و کنترباس', '2019', '2021-04-19 10:21:00', '2021-04-19 10:21:00'),
(17, NULL, 'Elegy for the spring for string quartet', '2019', 'audio/projects/607cf790ce51fal035_mind_against_walking_away_2981475022800627242.mp3', NULL, 'مرثیه بهاری برای کوارتت زهی', '2019', '2021-04-19 10:22:56', '2021-04-19 10:22:56'),
(18, NULL, 'Dilemma for chamber orchestra', '2019', NULL, NULL, 'معضلی برای ارکستر مجلسی', '2019', '2021-04-19 10:38:40', '2021-04-19 10:38:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
